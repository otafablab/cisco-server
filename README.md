# Notes of recovering a Cisco UCS C220 M3 BIOS

_Niklas Halonen and Joonas von Lerber, 8 February 2024_

## Introduction

This article contains notes and instructions on recovering a corrupted BIOS on a
Cisco UCS C220 M3 server. 

In addition, [this repository](https://gitlab.com/otafablab/cisco-server)
contains the firmware `cap` files for your convenience.

## Background

We acquired a Cisco UCS C220 M3 which was refusing to POST. Keyboard was not
detected and the only things displayed were "Configuring and testing memory..." and
"Configuring platform hardware..." The server also rebooted often.

After a while and after changing the recovery boot jumper (41) back and forth we
got a message that the BIOS was corrupt. The machine prompted us to insert a
thumbdrive containing a `recovery.cap` file at the root of it.

## Firmware download

We tried to find the aforementioned file but had a hard time at first. We found
the official download link, but it required logging in. We made multiple
accounts and managed to download the firmware for versions `3.0.4s` and
`1.4.6d`.

## Firmware extraction

The downloaded file is an ISO which can be mounted on linux using the following command

``` sh
𝝺 sudo mount -o loop ucs-c220-huu-3.0.4s.iso /mnt
mount: /mnt: WARNING: source write-protected, mounted read-only.
𝝺 ls -l /mnt
total 339175
dr-xr-xr-x 3 root root      2048 Jan  3  2017 EFI
-r--r--r-- 1 root root 223535136 Feb 19  2021 firmware.squashfs.enc
dr-xr-xr-x 2 root root      2048 Feb 19  2021 GETFW
-r--r--r-- 1 root root       225 Feb 19  2021 huu-release.xml
dr-xr-xr-x 2 root root      2048 Feb 19  2021 isolinux
dr-xr-xr-x 2 root root      2048 Feb 19  2021 LiveOS
-r--r--r-- 1 root root     20359 Feb 19  2021 Release-Notes-SL1.txt
-r--r--r-- 1 root root        33 Feb 19  2021 squashfs_img.enc.md5
-r--r--r-- 1 root root        33 Feb 19  2021 squashfs_img.md5
-r--r--r-- 1 root root     18007 Feb 19  2021 TOC_SANLUIS1.xml
-r--r--r-- 1 root root 123727904 Feb 19  2021 tools.squashfs.enc
dr-xr-xr-x 2 root root      2048 Feb 19  2021 VIC_FIRMWARE
```

> To unmount, run
>
> ```sh
> 𝝺 sudo umount /mnt
> ```

There are a couple of interesting files here.
- `GETFW/getfw`, a binary tool to decrypt the firmware
- `firmware.squashfs.enc`, the encrypted firmware itself

To decrypt the firmware without running the suspicion binary, I used the
following command

``` sh
openssl enc -aes-256-cbc -md md5 -d -in firmware.squashfs.enc -out ~/Desktop/firmware.squashfs -k "topspin@123cis@123co+"
```

This decrypts the file to `~/Desktop/firmware.squashfs`. This SquashFS file can then be unsquashed in the current directory using the following command

``` sh
𝝺 unsquashfs ~/Desktop/firmware.squashfs
𝝺 ls -l squashfs-root
total 41
drwxr-xr-x 2 niklash users   3 Feb 19  2021 bios
drwxr-xr-x 2 niklash users   8 Feb 19  2021 broadcom
drwxr-xr-x 2 niklash users   3 Feb 19  2021 cimc
drwxr-xr-x 6 niklash users   6 Feb 19  2021 emulex
drwxr-xr-x 2 niklash users   5 Feb 19  2021 fusionio
drwxr-xr-x 2 niklash users 174 Feb 19  2021 hdd
drwxr-xr-x 7 niklash users   9 Feb 19  2021 intel
drwxr-xr-x 7 niklash users   7 Feb 19  2021 lsi
drwxr-xr-x 6 niklash users   6 Feb 19  2021 qlogic
```

The bios recovery file is located at `bios/bios.cap`.

## USB thumbdrive

We created a master boot record partition table with one partition with a FAT32
(or FAT16) filesystem and moved the 3.0.4s firmware cap file to the partition's
root at `recovery.cap`.

> Refer to [this
> guide](https://www.digitalocean.com/community/tutorials/how-to-partition-and-format-storage-devices-in-linux)
> for instructions.

## Recovering the BIOS

The steps we took:
1. Power off the server and make sure the BIOS jumper is at the default position
2. Attach the USB thumbdrive
3. Power on the server; wait for the BIOS corrupt message to appear; wait for
   the BIOS to load (~5min)
   ![](./transferring.jpeg)
4. Immediately after loading the recovery BIOS, unplug the thumbdrive
5. Wait for the server to reconfigure and boot. This takes about 10min, be
   patient, don't turn on the server if it goes off by itself.
   ![](./configuring2.jpeg)
   
   ![](./cisco-boot.jpeg)

## Glossary of terms

| Term     | Explanation                                                                                                                            |
|----------|----------------------------------------------------------------------------------------------------------------------------------------|
| ISO      | Commonly a binary dump usually containing a filesystem or a partition table. The name is a reference to a legacy file system ISO 9660. |
| HUU      | Host upgrade utility. A Linux distribution? with various utilities to upgrade the firmware. The ISO file containing the firmware.      |
| SquashFS | A read-only compressed filesystem typically used for compact linux boot disks.                                                         |
| FAT      | File allocation table filesystem. Supported by most operating systems. Used for BIOS and UEFI related tasks.                           |

## Resources

- [Official software download for the server](https://software.cisco.com/download/home/284296253/type)
- [Firmware encryption password](https://community.cisco.com/t5/unified-computing-system-discussions/bios-update-package-for-ucs-c220m3-where-to-download-from/td-p/2941121/page/2)
- [Similar issue with corrupted BIOS](https://rsts11.com/category/cisco-2/cisco-ucs/)
